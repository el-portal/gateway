const express = require('express')
const proxy = require('express-http-proxy');
const { auth }  = require('express-openid-connect');
const {logger} = require('./logger')
const cors = require("cors");

const authEnv = process.env.AUTH === "true" ? true: false;
const port = process.env.GATEWAY_PORT;
const urlFront = 'http://' + process.env.FRONTEND_SERVICE + ':' + process.env.FRONTEND_PORT;
const urlWorldEntries = 'http://' + process.env.WORLD_ENTRIES_SERVICE + ':' + process.env.WORLD_ENTRIES_PORT;

const auth0Config = {
  authRequired: true,
  auth0Logout: true,
  secret: 'eryhretyhertyret',
  baseURL: 'http://localhost:5003',
  clientID: 'PslM4hHw89JFzwO4Jc1xqZX0IYoUswjb',
  issuerBaseURL: 'https://dev-bmz2sq4v.us.auth0.com',
}

process.on('uncaughtException', err => {
  logger.error({action: 'Fatal error', data:{error:err}});
  setTimeout(() => { process.exit(0) }, 1000).unref() 
})

process.on('unhandledRejection', (err, err2) => {
  logger.error({action: 'Fatal error', data:{error:err}});
  setTimeout(() => { process.exit(0) }, 1000).unref() 
})

process.on("SIGTERM", (err) => {
  logger.error({action: "Stopping...", data: {error: err}});
  setTimeout(() => { mprocess.exit(1);}, 100).unref();
});


const app = express();
app.use(cors());

// Parser de body y cookies
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

if(authEnv) {
  app.use(auth(auth0Config)); // Si existe la cookie del oidc entonces mete el user en el request context (Ademas captura /login, /logout y /callback)
}

app.get('/*',proxy(urlFront));
app.post('/world_entries/*',proxy(urlWorldEntries));

app.use((req, res) => {
  res.header("Content-Type", "application/json");
  res.writeHead(404);
  res.end("Page not found");
});

app.listen(port, function () { logger.info({ action: 'Service running on port ' + port}) } )
